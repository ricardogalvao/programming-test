# 1- Calculator App

This is a simple calculator application written in TypeScript that performs basic arithmetic operations on two numbers: addition, subtraction, multiplication, and division. The application can be run on any computer with Node.js installed, without requiring any additional installation.

## How to Use

1. **Clone the repository:**

   Clone this repository to your local machine using the following command:

   ```bash
   git clone git@gitlab.com:ricardogalvao/programming-test.git
   ```

2. **Navigate to the project folder:**
   ```bash
   cd calculator-app
   ```
3. **Install dependencies:**

   ```bash
   npm install
   ```

4. **Run the Calculator:**
   npm run start <num1> <operator> <num2>
   Replace <num1> and <num2> with the two numbers you want to calculate, and <operator> with one of the following:
   "+" (Addition)
   "-" (Subtraction)
   "x" (Multiplication)
   "div" (Division)

   Example: npm start 10 x 5

5. **Running Tests:**
   To run the unit tests
   ```bash
   npm test
   ```

# 2- Prime Numbers app

The Prime Number program is a command-line tool that finds and prints the first N prime numbers. The program is implemented in Node.js and TypeScript.

## How to Use

2.1 - **Use the sames steps of Calculator app, starting by step 2**.

2.2 - **Run the Prime Numberor:**
npm run start N
Replace N with a integer number;
Example: npm start 5

2.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 3- Factorial

The Factorial program is a command-line tool that calculates the factorial of a given number. The program is implemented in Node.js and TypeScript.

## How to Use

3.1 - **Use the sames steps of Calculator app, starting by step 2**.

3.2 - **Run the Facotrial:**
npm run start N
Replace N with a integer number;
Example: npm start 5

3.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 4- Palindrom

Create a function that checks whether a word is a palindrome (that is, whether it reads the
same backwards and forwards). The program must ask the user for a word and inform
whether or not it is a palindrome.

## How to Use

4.1 - **Use the sames steps of Calculator app, starting by step 2**.

4.2 - **Run the Palindrome:**
npm run start word
Replace word with a word to test if is palindrome;
Example: npm start radar

4.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 5-Table

This program takes a number from the user and displays the table of that number, from 1 to 10.

## How to Use

5.1 - **Use the sames steps of Calculator app, starting by step 2**.

5.2 - **Run the Table:**
npm run start N
Replace N with a integer number, to get its table;
Example: npm start 5

5.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 6-Vowel Counter

This program counts the number of vowels in a string. The user is prompted to enter a sentence, and the program will display how many vowels it has.

## How to Use

6.1 - **Use the sames steps of Calculator app, starting by step 2**.

6.2 - **Run the Vowel Counter:**
npm run start Sentence
Replace Sentece to vowel counter;
Example: npm start Hello word

6.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 7-Grade Average

This program takes a student's grades in three different subjects and calculates the average of the grades. It then displays the calculated average.

## How to Use

7.1 - **Use the sames steps of Calculator app, starting by step 2**.

7.2 - **Run the Grade Average:**
npm run start grade1 grade2 grade3
Replace gradex to corresponding grades;
Example: npm start 5.6 9.5 8.0

7.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

# 8-Investment Calculator

This program calculates the final value of an investment based on initial capital, interest rate, and investment time (in months). The program prompts the user for these values and displays the final value.

## How to Use

8.1 - **Use the sames steps of Calculator app, starting by step 2**.

8.2 - **Run the Grade Average:**
Amount: decimal
tax: decimal
period: integer(months)
npm run start amount tax period
Example: npm start 2500.00 12.5 12

8.3 - **Running Tests:**
To run the unit tests

```bash
   npm test
```

### Remember navigate to each folder of program, and run npm install before try run start.
