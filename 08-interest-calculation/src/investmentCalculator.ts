class InvestmentCalculator {
  calculateFinalValue(initialCapital: number, interestRate: number, investmentTimeMonths: number): number {
    if (initialCapital <= 0 || interestRate <= 0 || investmentTimeMonths <= 0) {
      throw new Error('Invalid input. Initial capital, interest rate, and investment time must be positive numbers.');
    }

    const interestRatePerMonth = interestRate / 100 / 12;
    const finalValue = initialCapital * Math.pow(1 + interestRatePerMonth, investmentTimeMonths);

    return finalValue;
  }
}

export default InvestmentCalculator;
