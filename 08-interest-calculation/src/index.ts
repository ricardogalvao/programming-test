import InvestmentCalculator from './investmentCalculator';

const investmentCalculator = new InvestmentCalculator();

const initialCapital = parseFloat(process.argv[2]);
const interestRate = parseFloat(process.argv[3]);
const investmentTimeMonths = parseFloat(process.argv[4]);

if (isNaN(initialCapital) || isNaN(interestRate) || isNaN(investmentTimeMonths)) {
  console.log('Invalid input. All values must be valid numbers.');
} else {
  const finalValue = investmentCalculator.calculateFinalValue(initialCapital, interestRate, investmentTimeMonths);
  console.log(`The final value of your investment will be: ${finalValue.toFixed(2)}`);
}
