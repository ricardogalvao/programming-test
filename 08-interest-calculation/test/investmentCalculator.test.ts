import InvestmentCalculator from '../src/investmentCalculator';

describe('InvestmentCalculator', () => {
  describe('calculateFinalValue', () => {
    it('should calculate the final value of an investment', () => {
      const calculator = new InvestmentCalculator();
      const initialCapital = 1000;
      const interestRate = 5;
      const investmentTimeMonths = 12;
      const finalValue = calculator.calculateFinalValue(initialCapital, interestRate, investmentTimeMonths);
      expect(finalValue).toBeCloseTo(1051.161897881733);
    });

    it('should throw an error for negative initial capital', () => {
      const calculator = new InvestmentCalculator();
      expect(() => calculator.calculateFinalValue(-1000, 5, 12)).toThrowError(
        'Invalid input. Initial capital, interest rate, and investment time must be positive numbers.'
      );
    });

    it('should throw an error for zero interest rate', () => {
      const calculator = new InvestmentCalculator();
      expect(() => calculator.calculateFinalValue(1000, 0, 12)).toThrowError(
        'Invalid input. Initial capital, interest rate, and investment time must be positive numbers.'
      );
    });

    it('should throw an error for negative investment time', () => {
      const calculator = new InvestmentCalculator();
      expect(() => calculator.calculateFinalValue(1000, 5, -12)).toThrowError(
        'Invalid input. Initial capital, interest rate, and investment time must be positive numbers.'
      );
    });
  });
});
