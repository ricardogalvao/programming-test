"use strict";
exports.__esModule = true;
var FactorialCalculator = /** @class */ (function () {
    function FactorialCalculator() {
    }
    FactorialCalculator.calculateFactorial = function (num) {
        if (num < 0) {
            throw new Error('Factorial is not defined for negative numbers.');
        }
        if (num === 0 || num === 1) {
            return 1;
        }
        var factorial = 1;
        for (var i = 2; i <= num; i++) {
            factorial *= i;
        }
        return factorial;
    };
    return FactorialCalculator;
}());
exports["default"] = FactorialCalculator;
