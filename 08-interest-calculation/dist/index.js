"use strict";
exports.__esModule = true;
var investmentCalculator_1 = require("./investmentCalculator");
var investmentCalculator = new investmentCalculator_1["default"]();
var initialCapital = parseFloat(process.argv[2]);
var interestRate = parseFloat(process.argv[3]);
var investmentTimeMonths = parseFloat(process.argv[4]);
if (isNaN(initialCapital) || isNaN(interestRate) || isNaN(investmentTimeMonths)) {
    console.log('Invalid input. All values must be valid numbers.');
}
else {
    var finalValue = investmentCalculator.calculateFinalValue(initialCapital, interestRate, investmentTimeMonths);
    console.log("The final value of your investment will be: ".concat(finalValue.toFixed(2)));
}
