"use strict";
exports.__esModule = true;
var InvestmentCalculator = /** @class */ (function () {
    function InvestmentCalculator() {
    }
    InvestmentCalculator.prototype.calculateFinalValue = function (initialCapital, interestRate, investmentTimeMonths) {
        if (initialCapital <= 0 || interestRate <= 0 || investmentTimeMonths <= 0) {
            throw new Error('Invalid input. Initial capital, interest rate, and investment time must be positive numbers.');
        }
        var interestRatePerMonth = interestRate / 100 / 12;
        var finalValue = initialCapital * Math.pow(1 + interestRatePerMonth, investmentTimeMonths);
        return finalValue;
    };
    return InvestmentCalculator;
}());
exports["default"] = InvestmentCalculator;
