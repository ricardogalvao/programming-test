"use strict";
exports.__esModule = true;
var vowelCounter_1 = require("./vowelCounter");
function main() {
    var args = process.argv.slice(2);
    var sentence = args.join(' ');
    if (!sentence) {
        console.log('Usage: npm start <sentence>');
        return;
    }
    var counter = new vowelCounter_1["default"]();
    var numVowels = counter.countVowels(sentence);
    console.log("Number of vowels in the sentence: ".concat(numVowels));
}
main();
