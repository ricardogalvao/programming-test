"use strict";
exports.__esModule = true;
var VowelCounter = /** @class */ (function () {
    function VowelCounter() {
    }
    VowelCounter.prototype.countVowels = function (sentence) {
        var vowels = 'aeiouAEIOU';
        var count = 0;
        for (var _i = 0, sentence_1 = sentence; _i < sentence_1.length; _i++) {
            var char = sentence_1[_i];
            if (vowels.includes(char)) {
                count++;
            }
        }
        return count;
    };
    return VowelCounter;
}());
exports["default"] = VowelCounter;
