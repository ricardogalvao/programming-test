"use strict";
// src/primeChecker.ts
exports.__esModule = true;
var PrimeChecker = /** @class */ (function () {
    function PrimeChecker() {
    }
    PrimeChecker.prototype.isPrime = function (num) {
        if (num <= 1) {
            return false;
        }
        for (var i = 2; i <= Math.sqrt(num); i++) {
            if (num % i === 0) {
                return false;
            }
        }
        return true;
    };
    PrimeChecker.findFirstNPrimes = function (n) {
        var primeCheker = new PrimeChecker();
        var primes = [];
        var num = 2;
        while (primes.length < n) {
            if (primeCheker.isPrime(num)) {
                primes.push(num);
            }
            num++;
        }
        return primes;
    };
    return PrimeChecker;
}());
exports["default"] = PrimeChecker;
