import VowelCounter from './vowelCounter';

function main() {
  const args = process.argv.slice(2);
  const sentence = args.join(' ');

  if (!sentence) {
    console.log('Usage: npm start <sentence>');
    return;
  }

  const counter = new VowelCounter();
  const numVowels = counter.countVowels(sentence);
  console.log(`Number of vowels in the sentence: ${numVowels}`);
}

main();
