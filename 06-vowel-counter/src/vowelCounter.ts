class VowelCounter {
  countVowels(sentence: string): number {
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    const lowerCaseSentence = sentence.toLowerCase(); // Convert the sentence to lowercase

    let vowelCount = 0;
    for (const char of lowerCaseSentence) {
      if (vowels.includes(char)) {
        vowelCount++;
      }
    }

    return vowelCount;
  }
}

export default VowelCounter;
