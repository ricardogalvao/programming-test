import VowelCounter from '../src/vowelCounter';

describe('VowelCounter', () => {
  describe('countVowels', () => {
    it('should return 0 for an empty string', () => {
      const counter = new VowelCounter();
      const result = counter.countVowels('');
      expect(result).toBe(0);
    });

    it('should return 0 for a string with no vowels', () => {
      const counter = new VowelCounter();
      const result = counter.countVowels('xyz123');
      expect(result).toBe(0);
    });

    it('should count all lowercase vowels in a sentence', () => {
      const counter = new VowelCounter();
      const result = counter.countVowels('Hello, how are you?');
      expect(result).toBe(7);
    });

    it('should count all uppercase vowels in a sentence', () => {
      const counter = new VowelCounter();
      const result = counter.countVowels('HELLO, HOW ARE YOU?');
      expect(result).toBe(7);
    });

    it('should count both lowercase and uppercase vowels in a sentence', () => {
      const counter = new VowelCounter();
      const result = counter.countVowels('Hello, HOW are YOU?');
      expect(result).toBe(7);
    });
  });
});
