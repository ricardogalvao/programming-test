class MultiplicationTable {
  private num: number;

  constructor(num: number) {
    this.num = num;
  }

  printTable(): void {
    for (let i = 1; i <= 10; i++) {
      const result = this.num * i;
      console.log(`${this.num} x ${i} = ${result}`);
    }
  }
}

export default MultiplicationTable;
