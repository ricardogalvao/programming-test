import MultiplicationTable from './multiplicationTable';

function main() {
  const num = process.argv[2];

  if (!num) {
    console.log('Usage: npm start <number>');
    return;
  }

  const parsedNum = parseInt(num, 10);

  if (isNaN(parsedNum)) {
    console.log('Invalid input. Please enter a valid number.');
    return;
  }

  const table = new MultiplicationTable(parsedNum);
  table.printTable();
}

main();
