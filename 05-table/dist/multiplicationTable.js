"use strict";
exports.__esModule = true;
var MultiplicationTable = /** @class */ (function () {
    function MultiplicationTable(num) {
        this.num = num;
    }
    MultiplicationTable.prototype.printTable = function () {
        for (var i = 1; i <= 10; i++) {
            var result = this.num * i;
            console.log("".concat(this.num, " x ").concat(i, " = ").concat(result));
        }
    };
    return MultiplicationTable;
}());
exports["default"] = MultiplicationTable;
