"use strict";
exports.__esModule = true;
var multiplicationTable_1 = require("./multiplicationTable");
function main() {
    var num = process.argv[2];
    if (!num) {
        console.log('Usage: npm start <number>');
        return;
    }
    var parsedNum = parseInt(num, 10);
    if (isNaN(parsedNum)) {
        console.log('Invalid input. Please enter a valid number.');
        return;
    }
    var table = new multiplicationTable_1["default"](parsedNum);
    table.printTable();
}
main();
