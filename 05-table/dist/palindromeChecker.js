"use strict";
exports.__esModule = true;
var PalindromeChecker = /** @class */ (function () {
    function PalindromeChecker(word) {
        this.word = word.toLowerCase();
    }
    PalindromeChecker.prototype.isPalindrome = function () {
        var reversed = this.word.split('').reverse().join('');
        return this.word === reversed;
    };
    PalindromeChecker.prototype.printResult = function () {
        if (this.isPalindrome()) {
            console.log("".concat(this.word, " is a palindrome."));
        }
        else {
            console.log("".concat(this.word, " is not a palindrome."));
        }
    };
    return PalindromeChecker;
}());
exports["default"] = PalindromeChecker;
