"use strict";
exports.__esModule = true;
var calculator_1 = require("./calculator");
function main() {
    var args = process.argv.slice(2);
    var normalizedOperator = '';
    if (args.length !== 3) {
        console.log('Usage: node index.js <num1> <operator> <num2>');
        return;
    }
    var num1 = args[0], operator = args[1], num2 = args[2];
    var parsedNum1 = parseFloat(num1);
    var parsedNum2 = parseFloat(num2);
    if (isNaN(parsedNum1) || isNaN(parsedNum2)) {
        console.log('Invalid input. Both numbers must be valid numbers.');
        return;
    }
    try {
        var result = calculator_1["default"].calculate(parsedNum1, parsedNum2, operator);
        console.log("Result: ".concat(result));
    }
    catch (error) {
        console.log(error.message);
    }
}
main();
