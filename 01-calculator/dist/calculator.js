"use strict";
exports.__esModule = true;
// calculator.ts
var Calculator = /** @class */ (function () {
    function Calculator() {
    }
    Calculator.prototype.add = function (num1, num2) {
        return num1 + num2;
    };
    Calculator.prototype.subtract = function (num1, num2) {
        return num1 - num2;
    };
    Calculator.prototype.multiply = function (num1, num2) {
        return num1 * num2;
    };
    Calculator.prototype.divide = function (num1, num2) {
        if (num2 === 0) {
            throw new Error('Division by zero is not allowed.');
        }
        return num1 / num2;
    };
    Calculator.prototype.normalizeOperator = function (operator) {
        if (operator.toLowerCase() === 'x') {
            return '*';
        }
        else if (operator === 'div') {
            return '/';
        }
        else {
            return operator;
        }
    };
    Calculator.calculate = function (num1, num2, operator) {
        var calculator = new Calculator(); // Create an instance of the Calculator class
        operator = calculator.normalizeOperator(operator);
        switch (operator) {
            case '+':
                return calculator.add(num1, num2);
            case '-':
                return calculator.subtract(num1, num2);
            case '*':
                return calculator.multiply(num1, num2);
            case '/':
                return calculator.divide(num1, num2);
            default:
                throw new Error('Invalid operator.');
        }
    };
    return Calculator;
}());
exports["default"] = Calculator;
