import Calculator from '../src/calculator';

describe('Calculator', () => {
  test('should add two numbers', () => {
    const calculator = new Calculator();
    const result = calculator.add(5, 10);
    expect(result).toBe(15);
  });

  test('should subtract two numbers', () => {
    const calculator = new Calculator();
    const result = calculator.subtract(10, 5);
    expect(result).toBe(5);
  });

  test('should multiply two numbers', () => {
    const calculator = new Calculator();
    const result = calculator.multiply(3, 7);
    expect(result).toBe(21);
  });

  test('should divide two numbers', () => {
    const calculator = new Calculator();
    const result = calculator.divide(20, 4);
    expect(result).toBe(5);
  });

  test('should throw an error for division by zero', () => {
    const calculator = new Calculator();
    expect(() => calculator.divide(10, 0)).toThrow('Division by zero is not allowed.');
  });
});
