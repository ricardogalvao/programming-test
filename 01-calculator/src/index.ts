import Calculator from './calculator';

function main() {
  const args = process.argv.slice(2);
  let normalizedOperator: string = '';

  if (args.length !== 3) {
    console.log('Usage: node index.js <num1> <operator> <num2>');
    return;
  }

  const [num1, operator, num2] = args;
  const parsedNum1 = parseFloat(num1);
  const parsedNum2 = parseFloat(num2);

  if (isNaN(parsedNum1) || isNaN(parsedNum2)) {
    console.log('Invalid input. Both numbers must be valid numbers.');
    return;
  }

  try {
    const result = Calculator.calculate(parsedNum1, parsedNum2, operator);
    console.log(`Result: ${result}`);
  } catch (error: any) {
    console.log(error.message);
  }
}

main();
