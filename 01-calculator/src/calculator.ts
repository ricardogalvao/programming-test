import { stringify } from 'querystring';

// calculator.ts
class Calculator {
  add(num1: number, num2: number): number {
    return num1 + num2;
  }

  subtract(num1: number, num2: number): number {
    return num1 - num2;
  }

  multiply(num1: number, num2: number): number {
    return num1 * num2;
  }

  divide(num1: number, num2: number): number {
    if (num2 === 0) {
      throw new Error('Division by zero is not allowed.');
    }
    return num1 / num2;
  }

  normalizeOperator(operator: string) {
    if (operator.toLowerCase() === 'x') {
      return '*';
    } else if (operator === 'div') {
      return '/';
    } else {
      return operator;
    }
  }

  static calculate(num1: number, num2: number, operator: string): number {
    const calculator = new Calculator(); // Create an instance of the Calculator class
    operator = calculator.normalizeOperator(operator);
    switch (operator) {
      case '+':
        return calculator.add(num1, num2);
      case '-':
        return calculator.subtract(num1, num2);
      case '*':
        return calculator.multiply(num1, num2);
      case '/':
        return calculator.divide(num1, num2);
      default:
        throw new Error('Invalid operator.');
    }
  }
}

export default Calculator;
