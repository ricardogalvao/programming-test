"use strict";
// src/index.ts
exports.__esModule = true;
var primeChecker_1 = require("./primeChecker");
function main() {
    var args = process.argv.slice(2);
    var n = parseInt(args[0], 10) || 10; // Default to 10 if no argument provided
    if (isNaN(n) || n <= 0) {
        console.log('Invalid input. Please provide a positive integer as an argument.');
        return;
    }
    var primes = primeChecker_1["default"].findFirstNPrimes(n);
    console.log("First ".concat(n, " prime numbers: ").concat(primes.join(', ')));
}
main();
