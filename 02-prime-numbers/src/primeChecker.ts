// src/primeChecker.ts

class PrimeChecker {
  isPrime(num: number): boolean {
    if (num <= 1) {
      return false;
    }

    for (let i = 2; i <= Math.sqrt(num); i++) {
      if (num % i === 0) {
        return false;
      }
    }

    return true;
  }

  static findFirstNPrimes(n: number): number[] {
    const primeCheker = new PrimeChecker();
    const primes: number[] = [];
    let num = 2;

    while (primes.length < n) {
      if (primeCheker.isPrime(num)) {
        primes.push(num);
      }
      num++;
    }

    return primes;
  }
}

export default PrimeChecker;
