// src/index.ts

import PrimeChecker from './primeChecker';

function main() {
  const args = process.argv.slice(2);
  const n = parseInt(args[0], 10) || 10; // Default to 10 if no argument provided

  if (isNaN(n) || n <= 0) {
    console.log('Invalid input. Please provide a positive integer as an argument.');
    return;
  }

  const primes = PrimeChecker.findFirstNPrimes(n);

  console.log(`First ${n} prime numbers: ${primes.join(', ')}`);
}

main();
