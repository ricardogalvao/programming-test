import PrimeChecker from '../src/primeChecker';

describe('PrimeChecker', () => {
  it('should correctly check if a number is prime', () => {
    const primeChecker = new PrimeChecker();

    expect(primeChecker.isPrime(2)).toBe(true);
    expect(primeChecker.isPrime(3)).toBe(true);
    expect(primeChecker.isPrime(5)).toBe(true);
    expect(primeChecker.isPrime(7)).toBe(true);
    expect(primeChecker.isPrime(11)).toBe(true);
    expect(primeChecker.isPrime(13)).toBe(true);
    expect(primeChecker.isPrime(4)).toBe(false);
    expect(primeChecker.isPrime(6)).toBe(false);
    expect(primeChecker.isPrime(9)).toBe(false);
    expect(primeChecker.isPrime(15)).toBe(false);
    expect(primeChecker.isPrime(25)).toBe(false);
  });

  it('should correctly find the first n prime numbers', () => {
    expect(PrimeChecker.findFirstNPrimes(5)).toEqual([2, 3, 5, 7, 11]);
    expect(PrimeChecker.findFirstNPrimes(10)).toEqual([2, 3, 5, 7, 11, 13, 17, 19, 23, 29]);
    expect(PrimeChecker.findFirstNPrimes(1)).toEqual([2]);
    expect(PrimeChecker.findFirstNPrimes(0)).toEqual([]);
  });

  it('should return the correct number of prime numbers', () => {
    const expectedPrimes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29];
    const n = expectedPrimes.length;
    const primes = PrimeChecker.findFirstNPrimes(n);
    expect(primes).toEqual(expectedPrimes);
  });
});
