"use strict";
exports.__esModule = true;
// src/index.ts
var gradesCalculator_1 = require("./gradesCalculator");
function main() {
    var args = process.argv.slice(2);
    if (args.length !== 3) {
        console.log('Usage: npm start <grade1> <grade2> <grade3>');
        return;
    }
    var grades = args.map(function (arg) { return parseFloat(arg); });
    var calculator = new gradesCalculator_1["default"]();
    try {
        var average = calculator.calculateAverage(grades);
        console.log("Average grade: ".concat(average.toFixed(2)));
    }
    catch (error) {
        console.log(error.message);
    }
}
main();
