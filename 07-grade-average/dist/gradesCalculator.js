"use strict";
exports.__esModule = true;
var GradesCalculator = /** @class */ (function () {
    function GradesCalculator() {
    }
    GradesCalculator.prototype.calculateAverage = function (grades) {
        if (grades.length !== 3) {
            throw new Error('Exactly three grades are required.');
        }
        if (grades.some(function (grade) { return isNaN(grade); })) {
            throw new Error('Invalid input. All grades must be valid numbers.');
        }
        var sum = grades.reduce(function (total, grade) { return total + grade; }, 0);
        return sum / grades.length;
    };
    return GradesCalculator;
}());
exports["default"] = GradesCalculator;
