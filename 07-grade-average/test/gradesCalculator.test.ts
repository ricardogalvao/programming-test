import GradesCalculator from '../src/gradesCalculator';

describe('GradesCalculator', () => {
  describe('calculateAverage', () => {
    it('should calculate the average of three valid grades', () => {
      const calculator = new GradesCalculator();
      const grades = [7.5, 8.2, 9.0];
      const average = calculator.calculateAverage(grades);
      expect(average).toBeCloseTo(8.233333333333333, 15);
    });

    it('should throw an error if exactly three grades are not provided', () => {
      const calculator = new GradesCalculator();
      const grades = [7.5, 8.2];
      expect(() => calculator.calculateAverage(grades)).toThrow('Exactly three grades are required.');
    });

    it('should throw an error if any of the grades is not a valid number', () => {
      const calculator = new GradesCalculator();
      const grades = [7.5, 8.2, NaN];
      expect(() => calculator.calculateAverage(grades)).toThrow('Invalid input. All grades must be valid numbers.');
    });
  });
});
