class GradesCalculator {
  calculateAverage(grades: number[]): number {
    if (grades.length !== 3) {
      throw new Error('Exactly three grades are required.');
    }

    if (grades.some((grade) => isNaN(grade))) {
      throw new Error('Invalid input. All grades must be valid numbers.');
    }

    const sum = grades.reduce((total, grade) => total + grade, 0);
    return sum / grades.length;
  }
}

export default GradesCalculator;
