// src/index.ts
import GradesCalculator from './gradesCalculator';

function main() {
  const args = process.argv.slice(2);

  if (args.length !== 3) {
    console.log('Usage: npm start <grade1> <grade2> <grade3>');
    return;
  }

  const grades = args.map((arg) => parseFloat(arg));

  const calculator = new GradesCalculator();
  try {
    const average = calculator.calculateAverage(grades);
    console.log(`Average grade: ${average.toFixed(2)}`);
  } catch (error: any) {
    console.log(error.message);
  }
}

main();
