class FactorialCalculator {
  static calculateFactorial(num: number): number {
    if (num < 0) {
      throw new Error('Factorial is not defined for negative numbers.');
    }

    if (num === 0 || num === 1) {
      return 1;
    }

    let factorial = 1;
    for (let i = 2; i <= num; i++) {
      factorial *= i;
    }

    return factorial;
  }
}

export default FactorialCalculator;
