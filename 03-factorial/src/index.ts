import FactorialCalculator from './factorialCalculator';

function main() {
  const args = process.argv.slice(2);

  if (args.length !== 1) {
    console.log('Usage: ts-node src/index.ts <number>');
    return;
  }

  const num = parseFloat(args[0]);

  if (isNaN(num)) {
    console.log('Invalid input. Please enter a valid number.');
    return;
  }

  try {
    const factorial = FactorialCalculator.calculateFactorial(num);
    console.log(`The factorial of ${num} is: ${factorial}`);
  } catch (error) {
    console.log(error.message);
  }
}

main();
