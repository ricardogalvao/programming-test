import FactorialCalculator from '../src/factorialCalculator';

describe('FactorialCalculator', () => {
  test('should calculate factorial correctly for non-negative numbers', () => {
    expect(FactorialCalculator.calculateFactorial(0)).toBe(1);
    expect(FactorialCalculator.calculateFactorial(1)).toBe(1);
    expect(FactorialCalculator.calculateFactorial(5)).toBe(120);
    expect(FactorialCalculator.calculateFactorial(10)).toBe(3628800);
  });

  test('should throw an error for negative numbers', () => {
    expect(() => FactorialCalculator.calculateFactorial(-1)).toThrowError('Factorial is not defined for negative numbers.');
  });

  test('should calculate the factorial of a large number', () => {
    // Mathematical value of 10!
    const expectedFactorial = 3628800;
    const result = FactorialCalculator.calculateFactorial(10);
    expect(result).toBe(expectedFactorial);
  });
});
