"use strict";
exports.__esModule = true;
var factorialCalculator_1 = require("./factorialCalculator");
function main() {
    var args = process.argv.slice(2);
    if (args.length !== 1) {
        console.log('Usage: ts-node src/index.ts <number>');
        return;
    }
    var num = parseFloat(args[0]);
    if (isNaN(num)) {
        console.log('Invalid input. Please enter a valid number.');
        return;
    }
    try {
        var factorial = factorialCalculator_1["default"].calculateFactorial(num);
        console.log("The factorial of ".concat(num, " is: ").concat(factorial));
    }
    catch (error) {
        console.log(error.message);
    }
}
main();
