import PalindromeChecker from '../src/palindromeChecker';

describe('PalindromeChecker', () => {
  test('should correctly identify palindromes', () => {
    const checker1 = new PalindromeChecker('radar');
    expect(checker1.isPalindrome()).toBe(true);

    const checker2 = new PalindromeChecker('level');
    expect(checker2.isPalindrome()).toBe(true);
  });

  test('should correctly identify non-palindromes', () => {
    const checker1 = new PalindromeChecker('hello');
    expect(checker1.isPalindrome()).toBe(false);

    const checker2 = new PalindromeChecker('world');
    expect(checker2.isPalindrome()).toBe(false);
  });

  test('should ignore case when checking palindromes', () => {
    const checker1 = new PalindromeChecker('Radar');
    expect(checker1.isPalindrome()).toBe(true);

    const checker2 = new PalindromeChecker('Level');
    expect(checker2.isPalindrome()).toBe(true);
  });

  test('should print correct result for palindromes', () => {
    const mockLog = jest.spyOn(console, 'log').mockImplementation(() => {});
    const checker = new PalindromeChecker('radar');
    checker.printResult();
    expect(mockLog).toHaveBeenCalledWith('radar is a palindrome.');
    mockLog.mockRestore();
  });

  test('should print correct result for non-palindromes', () => {
    const mockLog = jest.spyOn(console, 'log').mockImplementation(() => {});
    const checker = new PalindromeChecker('hello');
    checker.printResult();
    expect(mockLog).toHaveBeenCalledWith('hello is not a palindrome.');
    mockLog.mockRestore();
  });
});
