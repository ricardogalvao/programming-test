"use strict";
exports.__esModule = true;
// src/index.ts
var palindromeChecker_1 = require("./palindromeChecker");
function main() {
    var word = process.argv[2];
    if (!word) {
        console.log('Usage: npm start <word>');
        return;
    }
    var checker = new palindromeChecker_1["default"](word);
    checker.printResult();
}
main();
