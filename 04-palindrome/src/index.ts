// src/index.ts
import PalindromeChecker from './palindromeChecker';

function main() {
  const word = process.argv[2];

  if (!word) {
    console.log('Usage: npm start <word>');
    return;
  }

  const checker = new PalindromeChecker(word);
  checker.printResult();
}

main();
