class PalindromeChecker {
  private word: string;

  constructor(word: string) {
    this.word = word.toLowerCase();
  }

  isPalindrome(): boolean {
    const reversed = this.word.split('').reverse().join('');
    return this.word === reversed;
  }

  printResult(): void {
    if (this.isPalindrome()) {
      console.log(`${this.word} is a palindrome.`);
    } else {
      console.log(`${this.word} is not a palindrome.`);
    }
  }
}

export default PalindromeChecker;
